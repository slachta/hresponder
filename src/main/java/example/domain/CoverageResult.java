package example.domain;

import javax.persistence.*;

@Entity
@Table(name = "coverage")
public class CoverageResult {
    @Id
    @GeneratedValue()
    private long ID;

    @Column(nullable = false)
    private String DEVICE_ID;
    private double GPS_POSITION_LAT;
    private double GPS_POSITION_LONG;
    private double GPS_SPEED;
    private double GPS_VARIANCE;
    private int GSM_UMTS_ASU;
    private int GSM_UMTS_CELLID;
    private int GSM_UMTS_LAC;
    private int GSM_UMTS_RSSI;
    private int GSM_BER;
    private int UMTS_ECNO;
	private int LTE_ENB;
    private int LTE_OPERATOR_CODE;
    private String LTE_OPERATOR_NAME;
    private int LTE_PCELLID;
	private int LTE_SINR;
	private int LTE_RSRP;
    private int LTE_RSRQ;
    private int LTE_TAC;
    private String NETWORK_TYPE;
    private int TIMESTP;

    public CoverageResult(){

    }
    
    public int getLTE_SINR() {
		return LTE_SINR;
	}

	public void setLTE_SINR(int lTE_SINR) {
		LTE_SINR = lTE_SINR;
	}

    public int getLTE_PCELLID() {
		return LTE_PCELLID;
	}


	public void setLTE_PCELLID(int lTE_PCELLID) {
		LTE_PCELLID = lTE_PCELLID;
	}
    public double getGPS_POSITION_LAT() {
        return GPS_POSITION_LAT;
    }

    public void setGPS_POSITION_LAT(double prom) {
        GPS_POSITION_LAT = prom;
    }

    public double getGPS_POSITION_LONG() {
        return GPS_POSITION_LONG;
    }

    public void setGPS_POSITION_LONG(double prom) {
        GPS_POSITION_LONG = prom;
    }

    public double getGPS_SPEED() {
        return GPS_SPEED;
    }

    public void setGPS_SPEED(double prom) {
        GPS_SPEED = prom;
    }

    public double getGPS_VARIANCE() {
        return GPS_VARIANCE;
    }

    public void setGPS_VARIANCE(double prom) {
        GPS_VARIANCE = prom;
    }

    public int getGSM_UMTS_ASU() {
        return GSM_UMTS_ASU;
    }

    public void setGSM_UMTS_ASU(int prom) {
        GSM_UMTS_ASU = prom;
    }

    public int getGSM_UMTS_CELLID() {
        return GSM_UMTS_CELLID;
    }

    public void setGSM_UMTS_CELLID(int prom) {
        GSM_UMTS_CELLID = prom;
    }

    public int getUMTS_ECNO() {
		return UMTS_ECNO;
	}

	public void setUMTS_ECNO(int uMTS_ECNO) {
		UMTS_ECNO = uMTS_ECNO;
	}   
    public int getGSM_BER() {
		return GSM_BER;
	}

	public void setGSM_BER(int gSM_BER) {
		GSM_BER = gSM_BER;
	}
    public int getGSM_UMTS_LAC() {
        return GSM_UMTS_LAC;
    }

    public void setGSM_UMTS_LAC(int prom) {
        GSM_UMTS_LAC = prom;
    }

    public int getGSM_UMTS_RSSI() {
        return GSM_UMTS_RSSI;
    }

    public void setGSM_UMTS_RSSI(int prom) {
        GSM_UMTS_RSSI = prom;
    }

    public long getID_TEST() {
        return ID;
    }

    public void setID_TEST(long var) {
        ID = var;
    }

    public int getLTE_ENB() {
        return LTE_ENB;
    }

    public void setLTE_ENB(int var) {
        LTE_ENB = var;
    }

    public int getLTE_OPERATOR_CODE() {
        return LTE_OPERATOR_CODE;
    }

    public void setLTE_OPERATOR_CODE(int prom) {
        LTE_OPERATOR_CODE = prom;
    }

    public String getLTE_OPERATOR_NAME() {
        return LTE_OPERATOR_NAME;
    }

    public void setLTE_OPERATOR_NAME(String prom) {
        LTE_OPERATOR_NAME = prom;
    }

    public int getLTE_RSRP() {
        return LTE_RSRP;
    }

    public void setLTE_RSRP(int prom) {
        LTE_RSRP = prom;
    }

    public int getLTE_RSRQ() {
        return LTE_RSRQ;
    }

    public void setLTE_RSRQ(int prom) {
        LTE_RSRQ = prom;
    }

    public int getLTE_TAC() {
        return LTE_TAC;
    }

    public void setLTE_TAC(int prom) {
        LTE_TAC = prom;
    }

    public String getNETWORK_TYPE() {
        return NETWORK_TYPE;
    }

    public void setNETWORK_TYPE(String prom) {
        NETWORK_TYPE = prom;
    }

    public int getTIMESTP() {
        return TIMESTP;
    }

    public void setTIMESTP(int prom) {
        TIMESTP = prom;
    }

    public String getDEVICE_ID() {
        return DEVICE_ID;
    }

    public void setDEVICE_ID(String DEVICE_ID) {
        this.DEVICE_ID = DEVICE_ID;
    }
}