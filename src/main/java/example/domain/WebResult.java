package example.domain;

import javax.persistence.*;

@Entity
@Table(name = "webtest")
public class WebResult {
    @Id
    @GeneratedValue()
    private long ID;

    @Column(nullable = false)
    private String GUID;
    private String URL;
    private double SETUP_TIME;
    private double PRIMARY_LOAD_TIME;
    private double PRIMARY_PAGE_SIZE;
    private int TEST_TIME;
    private int PRIMARY_FILE_NUMBER;
    private int PRIMARY_IMAGE_NUMBER;
    private double PRIMARY_IMAGE_SIZE;
    private double PRIMARY_THROUGHPUT;
    private double SECONDARY_LOAD_TIME;
	private double SECONDARY_PAGE_SIZE;
    private int SECONDARY_FILE_NUMBER;
    private int SECONDARY_IMAGE_NUMBER;
	private double SECONDARY_IMAGE_SIZE;
	private double SECONDARY_THROUGHPUT;
	private double TOTAL_PAGE_SIZE;
    private double TOTAL_LOAD_TIME;
    private double TOTAL_THROUGHPUT;
    private int TOTAL_FILE_NUMBER;
    private int TIMESTP;

    public WebResult(){
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getGUID() {
        return GUID;
    }

    public void setGUID(String GUID) {
        this.GUID = GUID;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public double getSETUP_TIME() {
        return SETUP_TIME;
    }

    public void setSETUP_TIME(double SETUP_TIME) {
        this.SETUP_TIME = SETUP_TIME;
    }

    public double getPRIMARY_LOAD_TIME() {
        return PRIMARY_LOAD_TIME;
    }

    public void setPRIMARY_LOAD_TIME(double PRIMARY_LOAD_TIME) {
        this.PRIMARY_LOAD_TIME = PRIMARY_LOAD_TIME;
    }

    public double getPRIMARY_PAGE_SIZE() {
        return PRIMARY_PAGE_SIZE;
    }

    public void setPRIMARY_PAGE_SIZE(double PRIMARY_PAGE_SIZE) {
        this.PRIMARY_PAGE_SIZE = PRIMARY_PAGE_SIZE;
    }

    public int getTEST_TIME() {
        return TEST_TIME;
    }

    public void setTEST_TIME(int TEST_TIME) {
        this.TEST_TIME = TEST_TIME;
    }

    public int getPRIMARY_FILE_NUMBER() {
        return PRIMARY_FILE_NUMBER;
    }

    public void setPRIMARY_FILE_NUMBER(int PRIMARY_FILE_NUMBER) {
        this.PRIMARY_FILE_NUMBER = PRIMARY_FILE_NUMBER;
    }

    public int getPRIMARY_IMAGE_NUMBER() {
        return PRIMARY_IMAGE_NUMBER;
    }

    public void setPRIMARY_IMAGE_NUMBER(int PRIMARY_IMAGE_NUMBER) {
        this.PRIMARY_IMAGE_NUMBER = PRIMARY_IMAGE_NUMBER;
    }

    public double getPRIMARY_IMAGE_SIZE() {
        return PRIMARY_IMAGE_SIZE;
    }

    public void setPRIMARY_IMAGE_SIZE(double PRIMARY_IMAGE_SIZE) {
        this.PRIMARY_IMAGE_SIZE = PRIMARY_IMAGE_SIZE;
    }

    public double getPRIMARY_THROUGHPUT() {
        return PRIMARY_THROUGHPUT;
    }

    public void setPRIMARY_THROUGHPUT(double PRIMARY_THROUGHPUT) {
        this.PRIMARY_THROUGHPUT = PRIMARY_THROUGHPUT;
    }

    public double getSECONDARY_LOAD_TIME() {
        return SECONDARY_LOAD_TIME;
    }

    public void setSECONDARY_LOAD_TIME(double SECONDARY_LOAD_TIME) {
        this.SECONDARY_LOAD_TIME = SECONDARY_LOAD_TIME;
    }

    public double getSECONDARY_PAGE_SIZE() {
        return SECONDARY_PAGE_SIZE;
    }

    public void setSECONDARY_PAGE_SIZE(double SECONDARY_PAGE_SIZE) {
        this.SECONDARY_PAGE_SIZE = SECONDARY_PAGE_SIZE;
    }

    public int getSECONDARY_FILE_NUMBER() {
        return SECONDARY_FILE_NUMBER;
    }

    public void setSECONDARY_FILE_NUMBER(int SECONDARY_FILE_NUMBER) {
        this.SECONDARY_FILE_NUMBER = SECONDARY_FILE_NUMBER;
    }

    public int getSECONDARY_IMAGE_NUMBER() {
        return SECONDARY_IMAGE_NUMBER;
    }

    public void setSECONDARY_IMAGE_NUMBER(int SECONDARY_IMAGE_NUMBER) {
        this.SECONDARY_IMAGE_NUMBER = SECONDARY_IMAGE_NUMBER;
    }

    public double getSECONDARY_IMAGE_SIZE() {
        return SECONDARY_IMAGE_SIZE;
    }

    public void setSECONDARY_IMAGE_SIZE(double SECONDARY_IMAGE_SIZE) {
        this.SECONDARY_IMAGE_SIZE = SECONDARY_IMAGE_SIZE;
    }

    public double getSECONDARY_THROUGHPUT() {
        return SECONDARY_THROUGHPUT;
    }

    public void setSECONDARY_THROUGHPUT(double SECONDARY_THROUGHPUT) {
        this.SECONDARY_THROUGHPUT = SECONDARY_THROUGHPUT;
    }

    public double getTOTAL_PAGE_SIZE() {
        return TOTAL_PAGE_SIZE;
    }

    public void setTOTAL_PAGE_SIZE(double TOTAL_PAGE_SIZE) {
        this.TOTAL_PAGE_SIZE = TOTAL_PAGE_SIZE;
    }

    public double getTOTAL_LOAD_TIME() {
        return TOTAL_LOAD_TIME;
    }

    public void setTOTAL_LOAD_TIME(double TOTAL_LOAD_TIME) {
        this.TOTAL_LOAD_TIME = TOTAL_LOAD_TIME;
    }

    public double getTOTAL_THROUGHPUT() {
        return TOTAL_THROUGHPUT;
    }

    public void setTOTAL_THROUGHPUT(double TOTAL_THROUGHPUT) {
        this.TOTAL_THROUGHPUT = TOTAL_THROUGHPUT;
    }

    public int getTOTAL_FILE_NUMBER() {
        return TOTAL_FILE_NUMBER;
    }

    public void setTOTAL_FILE_NUMBER(int TOTAL_FILE_NUMBER) {
        this.TOTAL_FILE_NUMBER = TOTAL_FILE_NUMBER;
    }

    public int getTIMESTP() {
        return TIMESTP;
    }

    public void setTIMESTP(int TIMESTP) {
        this.TIMESTP = TIMESTP;
    }
}