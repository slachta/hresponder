package example.controler;

import com.google.gson.Gson;
import example.domain.CoverageResult;
import example.domain.WebResult;
import example.repository.TestResultRepository;
import example.repository.WebResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
public class RESTController {

    private final String apiPrefix = "api";
    @Autowired
    private TestResultRepository testResultRepository;
    @Autowired
    private WebResultRepository webResultRepository;

    @RequestMapping(value = apiPrefix+"/addMultipleCoverageTest", method = RequestMethod.POST)
    public ResponseEntity<String> addMultipleCoverageTests(@RequestBody String body) {
        try {
            Gson gson = new Gson();
            CoverageResult[] tr = gson.fromJson(body,CoverageResult[].class);

            Iterable<CoverageResult> iterable = Arrays.asList(tr);
            testResultRepository.save(iterable);

            return ResponseEntity.ok("Multiple coverage tests were added.");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Multiple coverage tests were not added.");
        }
    }

    @RequestMapping(value = apiPrefix+"/addCoverageTest", method = RequestMethod.POST)
    public ResponseEntity<String> addCoverageTest(@RequestBody String body) {
        try {
            System.out.println("adding a test");
            Gson gson = new Gson();
            CoverageResult tr = gson.fromJson(body,CoverageResult.class);

            testResultRepository.save(tr);

            return ResponseEntity.ok("Coverage test was added.");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Coverage test was not added.");
        }
    }

    @RequestMapping(value = apiPrefix+"/getCoverageTest/{testId}", method = RequestMethod.GET)
    public @ResponseBody CoverageResult getCoverageTest(@PathVariable("testId") long testId) {
        System.out.println("id "+testId);
        if(testResultRepository.exists(testId)) {
            System.out.println("found one");
            return testResultRepository.findOne(testId);
        }else{
            System.out.println("did not find one");
            return new CoverageResult();
        }
    }

    @RequestMapping(value = apiPrefix+"/getCoverageTestCount", method = RequestMethod.GET)
    @ResponseBody
    public long getCoverageTestCount() {
        return testResultRepository.count();
    }

    @RequestMapping(value = apiPrefix+"/addWebTest", method = RequestMethod.POST)
    public ResponseEntity<String> addWebTest(@RequestBody String body) {
        try {
            Gson gson = new Gson();
            CoverageResult tr = gson.fromJson(body,CoverageResult.class);

            testResultRepository.save(tr);

            return ResponseEntity.ok("Webtest was added.");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Webtest was added.");
        }
    }

    @RequestMapping(value = apiPrefix+"/addMultipleWebTest", method = RequestMethod.POST)
    public ResponseEntity<String> addMultipleWebTests(@RequestBody String body) {
        try {
            Gson gson = new Gson();
            WebResult[] tr = gson.fromJson(body,WebResult[].class);

            Iterable<WebResult> iterable = Arrays.asList(tr);
            webResultRepository.save(iterable);

            return ResponseEntity.ok("Webtests were added.");
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Webtests were not added.");
        }
    }

    @RequestMapping(value = apiPrefix+"/getWebTest/{testId}", method = RequestMethod.GET)
    public @ResponseBody WebResult getWebTest(@PathVariable("testId") long testId) {
        System.out.println("id "+testId);
        if(webResultRepository.exists(testId)) {
            System.out.println("found one");
            return webResultRepository.findOne(testId);
        }else{
            System.out.println("did not find one");
            return new WebResult();
        }
    }

    @RequestMapping(value = apiPrefix+"/getWebTestCount", method = RequestMethod.GET)
    @ResponseBody
    public long getWebTestCount() {
        return testResultRepository.count();
    }
}