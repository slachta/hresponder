package example.repository;

import example.domain.WebResult;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;


public interface WebResultRepository extends CrudRepository<WebResult, Long> {
    Page<WebResult> findAll();
}