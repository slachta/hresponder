package example.repository;

import example.domain.CoverageResult;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;

public interface TestResultRepository extends CrudRepository<CoverageResult, Long> {
    Page<CoverageResult> findAll();
}